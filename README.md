# Projet Lapin

Un projet qui permet de simuler la croissance des lapins (avec certaines règles) en utilisant des nombres aléatoires (venant du code Matsumoto Makoto).
Générateur de croissance de vie des lapins.

## Compilation

On lance le projet grâce à la commande :  
$ gcc -o simul *.c
Puis  
$ ./simul

## Auteurs
* **Cynthia LOURENCO** _alias_ [@cylourenco](https://gitlab.isima.fr/users/cylourenco)
* **Eldis YMERAJ** _alias_ [@elymereau](https://gitlab.isima.fr/users/elymereau)

